inferKimAndSimon <- function(mc, fdr, ado, fileNameBase, Blin=1000, Bmut=10000) {
  ## code adapted from Supplement of Kim and Simon, Using single cell 
  ## sequencing data to model the evolutionary history of a tumor, BMC 
  ## Bioinformatics, 2014
  
  dir.create(fileNameBase, recursive = TRUE, showWarnings = FALSE)
  
  p <- nrow(mc)
  n <- ncol(mc)
  
  mc[mc==2] <- NA
  
  ## 1. Marginal frequencies
  pairmat <- countpairs(mc)
  csummat <- colSums(pairmat)
  fij <- csummat/sum(csummat)
  
  ## 2. Optimize alpha
  for (PARAM in 0:99) {
    
    seed <- 20120528 + PARAM
    set.seed(seed)
    
    alpha <- PARAM/100
    mprob <- genprior.c(n=n, alpha=alpha,
                        fdr=fdr, ado=ado, Blin=Blin, Bmut=Bmut)$mp
    
    save(mprob, file=paste0(fileNameBase,sprintf("/mprob-alpha-%02d.RData", PARAM)))
  }
  
  ## 3. ssmp-alpha.#
  # $ ssmp-alpha.R #generate 1 .pdf file
  pij <- matrix(, 9, 100)
  for (i in 1:100) {
    load(paste0(fileNameBase,sprintf("/mprob-alpha-%02d.RData", i-1)))
    pij[,i] <- mprob
  }
  ssmp <- ssmpdiff(fij, pij)
  optalpha <- (which.min(ssmp) - 1)/100
  
  ## 4. Generate prior
  prior <- genprior.c(n=n, alpha=optalpha,
                      fdr=fdr, ado=ado, Blin=Blin, Bmut=Bmut)
  
  ## 5. Calculate posterior
  posterior <- computeposterior(pairmat, prior)
  assoc <- sign(posterior - apply(posterior, 1, max))
  
  G <- new("graphNEL", nodes=as.character(1:p), edgemode="directed")
  npair <- p*(p-1)/2
  
  sel.pair <- matrix(, npair, 2)
  k <- 1
  for (i in 1:(p-1)) {
    for (j in (i+1):p) {
      sel.pair[k,1] <- as.character(i)
      sel.pair[k,2] <- as.character(j)
      k <- k + 1
    }
  }
  
  for (k in 1:npair) {
    v1 <- sel.pair[k,1]
    v2 <- sel.pair[k,2]
    w1 <- -log(posterior[k,1])
    w2 <- -log(posterior[k,2])
    if (assoc[k,1] == 0) {
      G <- addEdge(v1, v2, G, w1)
    }
    if (assoc[k,2] == 0) {
      G <- addEdge(v2, v1, G, w2)
    }
  }
  
  ## Edmonds' algorithm for minimum total weights
  Gob <- eobmin(G)
  
  return(Gob)
  
}